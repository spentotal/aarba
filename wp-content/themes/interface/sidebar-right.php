<?php
/**
 * Displays the right sidebar of the theme.
 *
 * @package Theme Horse
 * @subpackage Interface
 * @since Interface 1.0
 */
?>
<?php
	/**
	 * interface_before_right_sidebar
	 */
	do_action( 'interface_before_right_sidebar' );
?>
<?php
	/** 
	 * interface_right_sidebar hook
	 *
	 * HOOKED_FUNCTION_NAME PRIORITY
	 *
	 * interface_display_right_sidebar 10
	 */
	do_action( 'interface_right_sidebar' );
?>
<?php
	/**
	 * interface_after_right_sidebar
	 */
	do_action( 'interface_after_right_sidebar' );
?>

<div id="sidebar-wrapper">
	<?php

		$defaults = array(
			'theme_location'  => '',
			'menu'            => 'all',
			'container'       => 'div',
			'container_class' => 'sidebar-nav',
			'container_id'    => '',
			'menu_class'      => 'sub-menu navbar navbar-default ',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<nav id="%1$s" class="%2$s" role="navigation">%3$s</nav>',
			'depth'           => 2,
			'level'			  => 1,	
			'walker'          => ''
		);

		wp_nav_menu( $defaults );

		

	?>
</div>