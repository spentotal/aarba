<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to interface_comment() which is
 * located in the content-extensions.php file under the function folder inside inc folder.
 *
 * @package Theme Horse
 * @subpackage Interface
 * @since Interface 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>

<!-- #comments .comments-area -->